# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Estados(models.Model):
    es_id = models.IntegerField(primary_key=True)
    es_desc = models.CharField(max_length=80)

    class Meta:
        db_table = 'Estados'


class Personas(models.Model):
    per_id = models.IntegerField(primary_key=True)
    per_nombre = models.CharField(max_length=100)
    per_apellido = models.CharField(max_length=100)
    per_cedula = models.CharField(max_length=10)
    per_calle = models.CharField(max_length=150, blank=True, null=True)
    per_telefono = models.TextField(blank=True, null=True)
    per_email = models.CharField(max_length=60, blank=True, null=True)
    per_fecha_nacimiento = models.DateField(blank=True, null=True)
    fecha_alta = models.DateField()
    usuario_alta = models.IntegerField()
    fecha_mod = models.DateField(blank=True, null=True)
    usuario_mod = models.IntegerField(blank=True, null=True)
    es = models.ForeignKey(Estados, models.DO_NOTHING)

    class Meta:
        db_table = 'Personas'


class Prioridades(models.Model):
    pri_id = models.IntegerField(primary_key=True)
    pri_desc = models.CharField(max_length=50)

    class Meta:
        db_table = 'Prioridades'


class Roles(models.Model):
    rol_id = models.IntegerField(primary_key=True)
    rol_desc = models.CharField(max_length=100)

    class Meta:
        db_table = 'Roles'


class Servicios(models.Model):
    ser_id = models.IntegerField(primary_key=True)
    ser_desc = models.CharField(max_length=80)
    fecha_alta = models.DateField()
    usuario_alta = models.IntegerField()
    fecha_mod = models.DateField(blank=True, null=True)
    usuario_mod = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'Servicios'

class Usuarios(models.Model):
    usu_id = models.IntegerField(primary_key=True)
    usu_desc = models.CharField(max_length=50)
    usu_pass = models.CharField(max_length=50)
    per = models.ForeignKey(Personas, models.DO_NOTHING)
    rol = models.ForeignKey(Roles, models.DO_NOTHING)
    fecha_alta = models.DateField()
    usuario_alta = models.IntegerField()
    fecha_mod = models.DateField(blank=True, null=True)
    usuario_mod = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'Usuarios'

class Terminales(models.Model):
    ter_id = models.IntegerField(primary_key=True)
    ter_desc = models.CharField(max_length=50)
    ser = models.ForeignKey(Servicios, models.DO_NOTHING)
    usu = models.ForeignKey('Usuarios', models.DO_NOTHING)

    class Meta:
        db_table = 'Terminales'


class Tickets(models.Model):
    ticket_id = models.IntegerField(primary_key=True)
    ticket_desc = models.CharField(max_length=100)
    es = models.ForeignKey(Estados, models.DO_NOTHING)

    class Meta:
        db_table = 'Tickets'


class TipoCliente(models.Model):
    tc_id = models.IntegerField(primary_key=True)
    tc_desc = models.CharField(max_length=100)

    class Meta:
        db_table = 'Tipo_cliente'

class Clientes(models.Model):
    cli_id = models.IntegerField(primary_key=True)
    per = models.ForeignKey('Personas', models.DO_NOTHING)
    tc = models.ForeignKey('TipoCliente', models.DO_NOTHING)
    es = models.ForeignKey('Estados', models.DO_NOTHING)
    cli_razon_social = models.CharField(max_length=100, blank=True, null=True)
    cli_ruc = models.CharField(max_length=20, blank=True, null=True)
    fecha_alta = models.DateField()
    usuario_alta = models.IntegerField()
    fecha_mod = models.DateField(blank=True, null=True)
    usuario_mod = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'Clientes'


class Colas(models.Model):
    col_id = models.IntegerField(primary_key=True)
    col_desc = models.CharField(max_length=100)
    cli_id = models.IntegerField()
    pri = models.ForeignKey('Prioridades', models.DO_NOTHING)
    ser = models.ForeignKey('Servicios', models.DO_NOTHING)
    ticket = models.ForeignKey('Tickets', models.DO_NOTHING)
    ter = models.ForeignKey('Terminales', models.DO_NOTHING)
    fecha_entrada = models.DateTimeField()
    fecha_salida = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'Colas'